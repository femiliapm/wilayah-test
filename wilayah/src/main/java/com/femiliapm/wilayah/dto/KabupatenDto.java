package com.femiliapm.wilayah.dto;

public class KabupatenDto {
	private String code;
	private String name;
	private String codeProvinsi;
	private String nameProvinsi;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCodeProvinsi() {
		return codeProvinsi;
	}

	public void setCodeProvinsi(String codeProvinsi) {
		this.codeProvinsi = codeProvinsi;
	}

	public String getNameProvinsi() {
		return nameProvinsi;
	}

	public void setNameProvinsi(String nameProvinsi) {
		this.nameProvinsi = nameProvinsi;
	}
}
