package com.femiliapm.wilayah.dto;

public class DesaDto {
	private String code;
	private String name;
	private String codeKecamatan;
	private String nameKecamatan;
	private String codeKabupaten;
	private String nameKabupaten;
	private String codeProvinsi;
	private String nameProvinsi;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCodeKecamatan() {
		return codeKecamatan;
	}

	public void setCodeKecamatan(String codeKecamatan) {
		this.codeKecamatan = codeKecamatan;
	}

	public String getNameKecamatan() {
		return nameKecamatan;
	}

	public void setNameKecamatan(String nameKecamatan) {
		this.nameKecamatan = nameKecamatan;
	}

	public String getCodeKabupaten() {
		return codeKabupaten;
	}

	public void setCodeKabupaten(String codeKabupaten) {
		this.codeKabupaten = codeKabupaten;
	}

	public String getNameKabupaten() {
		return nameKabupaten;
	}

	public void setNameKabupaten(String nameKabupaten) {
		this.nameKabupaten = nameKabupaten;
	}

	public String getCodeProvinsi() {
		return codeProvinsi;
	}

	public void setCodeProvinsi(String codeProvinsi) {
		this.codeProvinsi = codeProvinsi;
	}

	public String getNameProvinsi() {
		return nameProvinsi;
	}

	public void setNameProvinsi(String nameProvinsi) {
		this.nameProvinsi = nameProvinsi;
	}
}
