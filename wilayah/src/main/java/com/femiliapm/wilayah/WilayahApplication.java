package com.femiliapm.wilayah;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WilayahApplication {

	public static void main(String[] args) {
		SpringApplication.run(WilayahApplication.class, args);
	}

}
