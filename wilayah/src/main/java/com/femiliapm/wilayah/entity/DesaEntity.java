package com.femiliapm.wilayah.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_desa")
public class DesaEntity {
	@Id
	@Column(name = "desaId", length = 25)
	private String kodeDesa;
	
	@Column(name = "desaName")
	private String namaDesa;
	
	@ManyToOne
	@JoinColumn(name = "kecId", nullable = false)
	private KecamatanEntity kecamatanEntity;
	
	@ManyToOne
	@JoinColumn(name = "kabId", nullable = false)
	private KabupatenEntity kabupatenEntity;
	
	@ManyToOne
	@JoinColumn(name = "provId", nullable = false)
	private ProvinsiEntity provinsiEntity;

	public String getKodeDesa() {
		return kodeDesa;
	}

	public void setKodeDesa(String kodeDesa) {
		this.kodeDesa = kodeDesa;
	}

	public String getNamaDesa() {
		return namaDesa;
	}

	public void setNamaDesa(String namaDesa) {
		this.namaDesa = namaDesa;
	}

	public KecamatanEntity getKecamatanEntity() {
		return kecamatanEntity;
	}

	public void setKecamatanEntity(KecamatanEntity kecamatanEntity) {
		this.kecamatanEntity = kecamatanEntity;
	}

	public KabupatenEntity getKabupatenEntity() {
		return kabupatenEntity;
	}

	public void setKabupatenEntity(KabupatenEntity kabupatenEntity) {
		this.kabupatenEntity = kabupatenEntity;
	}

	public ProvinsiEntity getProvinsiEntity() {
		return provinsiEntity;
	}

	public void setProvinsiEntity(ProvinsiEntity provinsiEntity) {
		this.provinsiEntity = provinsiEntity;
	}
}
