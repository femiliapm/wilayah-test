package com.femiliapm.wilayah.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "t_kecamatan")
public class KecamatanEntity {
	@Id
	@Column(name = "kecId", length = 25)
	private String kodeKecamatan;
	
	@Column(name = "kecName")
	private String namaKecamatan;
	
	@ManyToOne
	@JoinColumn(name = "kabId", nullable = false)
	private KabupatenEntity kabupatenEntity;
	
	@ManyToOne
	@JoinColumn(name = "provId", nullable = false)
	private ProvinsiEntity provinsiEntity;

	public String getKodeKecamatan() {
		return kodeKecamatan;
	}

	public void setKodeKecamatan(String kodeKecamatan) {
		this.kodeKecamatan = kodeKecamatan;
	}

	public String getNamaKecamatan() {
		return namaKecamatan;
	}

	public void setNamaKecamatan(String namaKecamatan) {
		this.namaKecamatan = namaKecamatan;
	}

	public KabupatenEntity getKabupatenEntity() {
		return kabupatenEntity;
	}

	public void setKabupatenEntity(KabupatenEntity kabupatenEntity) {
		this.kabupatenEntity = kabupatenEntity;
	}

	public ProvinsiEntity getProvinsiEntity() {
		return provinsiEntity;
	}

	public void setProvinsiEntity(ProvinsiEntity provinsiEntity) {
		this.provinsiEntity = provinsiEntity;
	}
}
