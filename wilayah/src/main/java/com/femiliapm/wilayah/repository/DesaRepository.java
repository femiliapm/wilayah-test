package com.femiliapm.wilayah.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.femiliapm.wilayah.entity.DesaEntity;

@Repository
public interface DesaRepository extends JpaRepository<DesaEntity, String>{

}
