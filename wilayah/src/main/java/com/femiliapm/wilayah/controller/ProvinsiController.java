package com.femiliapm.wilayah.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.femiliapm.wilayah.dto.ProvinsiDto;
import com.femiliapm.wilayah.entity.ProvinsiEntity;
import com.femiliapm.wilayah.repository.ProvinsiRepository;

@RestController
@RequestMapping("/provinsi")
public class ProvinsiController {
	private final ProvinsiRepository provinsiRepository;

	@Autowired
	public ProvinsiController(ProvinsiRepository provinsiRepository) {
		this.provinsiRepository = provinsiRepository;
	}

	@GetMapping
	public List<ProvinsiDto> get() {
		List<ProvinsiEntity> provinsiEntities = provinsiRepository.findAll();
		List<ProvinsiDto> provinsiDtos = provinsiEntities.stream().map(this::convertToDto).collect(Collectors.toList());
		return provinsiDtos;
	}

	@GetMapping("/{code}")
	public ProvinsiDto get(@PathVariable String code) {
		if (provinsiRepository.findById(code).isPresent()) {
			ProvinsiDto provinsiDto = convertToDto(provinsiRepository.findById(code).get());
			return provinsiDto;
		}
		return null;
	}

	@PostMapping
	public ProvinsiDto insert(@RequestBody ProvinsiDto dto) {
		ProvinsiEntity provinsiEntity = convertToEntity(dto);
		provinsiRepository.save(provinsiEntity);
		return dto;
	}

	private ProvinsiEntity convertToEntity(ProvinsiDto dto) {
		ProvinsiEntity provinsiEntity = new ProvinsiEntity();
		provinsiEntity.setKodeProvinsi(dto.getCode());
		provinsiEntity.setNamaProvinsi(dto.getName());
		return provinsiEntity;
	}

	private ProvinsiDto convertToDto(ProvinsiEntity provinsiEntity) {
		ProvinsiDto provinsiDto = new ProvinsiDto();
		provinsiDto.setCode(provinsiEntity.getKodeProvinsi());
		provinsiDto.setName(provinsiEntity.getNamaProvinsi());
		return provinsiDto;
	}
}
